import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class Task5Test {

    @Test
    public void CountSaleItems() {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get( "https://www.etsy.com/?ref=lgo" );
            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

            Actions actions = new Actions(webDriver);
            WebElement clothesAndShoesMenu = webDriver.findElement(By.linkText("Clothing & Shoes"));
            actions.moveToElement(clothesAndShoesMenu).build().perform();

            WebDriverWait waitmen = new WebDriverWait(webDriver, 10);
            waitmen.until(ExpectedConditions.visibilityOfElementLocated(By.xpath( "//*[@id=\"desktop-category-nav\"]/div[2]/div/div[2]/div/div/aside/ul/li[2]" )));

            WebElement mensMenu = webDriver.findElement(By.xpath("//*[@id=\"desktop-category-nav\"]/div[2]/div/div[2]/div/div/aside/ul/li[2]"));
            actions.moveToElement(mensMenu).build().perform();

            WebDriverWait waitboots = new WebDriverWait(webDriver, 10);
            waitboots.until(ExpectedConditions.visibilityOfElementLocated(By.linkText( "Boots" )));


            WebElement menShooesMenu = webDriver.findElement( By.linkText( "Boots" ) );
            actions.moveToElement( menShooesMenu );
            actions.click().build().perform();
            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

            WebElement saleMenu = webDriver.findElement( By.cssSelector( "#search-filter-reset-form > div:nth-child(2) > fieldset > div > " +
                    "div > div:nth-child(2) > div > a > label" ) );
            saleMenu.click();

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

            // count sale items in men's boots on the first page

            List<WebElement> categoryElement = webDriver.findElements(By.cssSelector("#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 >" +
                    " div > div.wt-mt-xs-2.wt-text-black > div.col-group.pl-xs-0.search-listings-group.pr-xs-1 > div:nth-child(2) >" +
                    " div.bg-white.display-block.pb-xs-2.mt-xs-0 > div > div > ul > li"));
            System.out.println(categoryElement.size());

            // check the number of sale items is the same on site results

//            String expectedSaleItems= "1,744 results,";
//            assertEquals( expectedSaleItems, webDriver.findElement( By.cssSelector( "#content > div > div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > " +
//                    "div > div.mt-xs-2.pl-xs-1.pl-md-4.pl-lg-6.pr-xs-1.pr-md-4.pr-lg-6 > div > span > span > span:nth-child(2)" ) ).getText() );

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
        } finally {
            webDriver.close();
        }
    }
}

