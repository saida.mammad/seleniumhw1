import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.*;


import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Task1Test {
    @Test
    public void CheckWebsite() {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        WebDriverWait webDriverWait = new WebDriverWait( webDriver, 10 );

        try {
            webDriver.get( "https://www.google.com/" );

            WebElement searchField = webDriver.findElement( By.xpath( "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input" ) );
            searchField.sendKeys( "Selenium" );
            WebElement buttonElement = webDriver.findElement( By.xpath( "/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]" ) );
            buttonElement.click();
            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
            WebElement findLink = webDriver.findElement( By.xpath( "//*[@id=\"rso\"]/div/div[1]/div/div[1]/a/h3" ) );
            findLink.click();

            String expectedHeader = "Selenium automates browsers. That's it!";
            assertEquals( expectedHeader, webDriver.findElement( By.cssSelector( "body > section.hero.homepage > h1:nth-child(1)" ) ).getText() );

        } finally {
            webDriver.close();
        }

    }

    @Test
    public void LoginUserPositive() {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get( "https://limak.az/az" );

            WebElement enterButton = webDriver.findElement( By.cssSelector( "body > header > nav.navbar.navbar-default.nav-top > div > " +
                    "ul.nav.navbar-nav.navbar-right > li:nth-child(1) > a" ) );
            enterButton.click();
            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

            WebElement userNameElement = webDriver.findElement( By.cssSelector( "body > header > nav.navbar.navbar-default.nav-top > div >" +
                    " ul.nav.navbar-nav.navbar-right > li:nth-child(1) > div > div > div > form > div > div.login-left.col-sm-6.col-xs-12 > " +
                    "div:nth-child(2) > label > input" ) );
            userNameElement.sendKeys( "saida.mammad@gmail.com" );

            WebElement passwordElement = webDriver.findElement( By.cssSelector( "body > header > nav.navbar.navbar-default.nav-top > div >" +
                    " ul.nav.navbar-nav.navbar-right > li:nth-child(1) > div > div > div > form > div > div.login-left.col-sm-6.col-xs-12 > " +
                    "div:nth-child(3) > label > input" ) );
            passwordElement.sendKeys( "Data@@2019" );
            passwordElement.submit();

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
            webDriver.findElement( By.cssSelector( "#app-layout > section > div.limac-modal.show-modal > div > div > div > button > img" ) ).click();

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
            String expectedHeader = "Saida Salimova";
            assertEquals( expectedHeader, webDriver.findElement( By.cssSelector( "body > header > nav.navbar.navbar-default.nav-top > div >" +
                    " ul.nav.navbar-nav.navbar-right.logined-menu > li > a" ) ).getText() );

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

        } finally {
            webDriver.close();
        }
    }

    @Test
    public void LoginUserNegative() {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get( "https://www.instagram.com/" );
            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

            WebElement userNameElement = webDriver.findElement( By.cssSelector( "#loginForm > div.Igw0E.IwRSH.eGOV_._4EzTm.kEKum > div:nth-child(1) > " +
                    "div > label > input" ) );
            userNameElement.sendKeys( "saida_salimli" );

            WebElement passwordElement = webDriver.findElement( By.cssSelector( "#loginForm > div.Igw0E.IwRSH.eGOV_._4EzTm.kEKum > div:nth-child(2) > " +
                    "div > label > input" ) );
            passwordElement.sendKeys( "Bank123456789" );
            passwordElement.submit();
            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

            String expectedHeader = "Sorry, your password was incorrect. Please double-check your password.";
            assertEquals( expectedHeader, webDriver.findElement( By.cssSelector( "#slfErrorAlert" ) ).getText() );

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );

        } finally {
            webDriver.close();
        }
    }
}
