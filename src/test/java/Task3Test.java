import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class Task3Test {

    @Test
    public void UploadFile() {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get( "https://seleniumtestsgroup.slack.com/" );

            WebElement userNameElement = webDriver.findElement( By.cssSelector( "#email" ) );
            userNameElement.sendKeys( "saida.tests@gmail.com" );

            WebElement passwordElement = webDriver.findElement( By.cssSelector( "#password" ) );
            passwordElement.sendKeys( "Data@@2019" );
            passwordElement.submit();

            Thread.sleep( 5000 );

            Alert alert = webDriver.switchTo().alert();
            Thread.sleep( 5000 );
            alert.dismiss();

            Thread.sleep( 5000 );

            WebElement webSlack = webDriver.findElement( By.cssSelector("#page_contents > div > div > div.p-ssb_redirect__body.full_height.align_center > " +
                    "p > a" ) );
            webSlack.click();
            webDriver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS );


            WebElement uploadFileIcon = webDriver.findElement( By.cssSelector( "body > div.p-client_container > div > div.p-workspace-layout > " +
                    "div.p-workspace__primary_view > div > div.p-file_drag_drop__container > div.p-workspace__primary_view_footer > div > div > " +
                    "div.p-workspace__input.p-message_input > div.c-texty_input__container.c-texty_input__container--multi_line.c-texty_input__container--sticky_composer > " +
                    "div.c-texty_buttons > button:nth-child(2)" ) );
            uploadFileIcon.click();

            WebElement uploadFileComputor = webDriver.findElement( By.cssSelector( "#menu-69-1 > div.c-menu_item__label > div > div" ) );
            uploadFileComputor.sendKeys( "C:\\Users\\004558\\Desktop\\for test.txt" );
            uploadFileComputor.submit();

            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );



//            String expectedHeader = "Daxil ol";
//            assertEquals( expectedHeader, webDriver.findElement( By.cssSelector( "#page_contents > div > div > div.p-ssb_redirect__body.full_height.align_center > " +
//                    "p > a" ) ).getText() );
//
//            webDriver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }
    }
}
